#SCHEDULER DI PROCESSI

.data
#INDIRIZZI SALVATI:
ptrTestaLista: .word 0	#indirizzo testa della lista dei task
ptrCodaLista: .word 0	#indirizzo coda della lista dei tak
ptrTestaGarbage: .word 0    #indirizzo testa Garbage Collector
strPolicy: .ascii "Policy selezionata: "
policy: .asciiz "a"                #policy ordinamento Task (a= per priorità  b= per n di esecuzioni)
id_max:	.byte 0          #ID massimo inserito
.align 2			#allineo ad un indirizzo multiplo di 4 (2^2)

#STRINGHE PER IL MENU:
strMenu: .ascii "Operazioni disponibili:\n"
str1: .ascii "1) Inserire un nuovo Task\n"
str2: .ascii "2) Eseguire il primo Task\n"
str3: .ascii "3) Eseguire un Task a scelta\n"
str4: .ascii "4) Eliminare un Task a scelta\n"
str5: .ascii "5) Modificare la priorità di un Task\n"
str6: .ascii "6) Cambiare la politica di scheduling\n"
str7: .ascii "7) Uscire dal programma\n\n"

#STRINGHE DI INTERAZIONE MENU:
strChoose: .asciiz "Inserire il numero dell'istruzione da eseguire:  "
strErr:	.asciiz "Errore! Inserire un numero tra 1 e 7.\n\n"
strACapo: .asciiz "\n"
strExit: .asciiz "\n\nProgramma Terminato!\n\n"
chooseBufferOne: .space 2	#per memorizzare la scelta da 1 char
chooseBufferTwo: .space 3	#per memorizzare scelta da 2 char
.align 2			#allineo ad un indirizzo multiplo di 4 (2^2)

#MEMORIZZAZIONE DATI MENU:
strJAT: .word choose_1, choose_2, choose_3, choose_4, choose_5, choose_6, choose_7, choose_8

#MEMORIA E STRINGHE PER CARICAMENTO DA FILE:
strFnF:	 .ascii  "The file was not found: "
strFile: .asciiz "task_prefabbricati.txt"
taskBuffer: .space 184	#Per inserire in memoria 10 task
strBuffer: .space 9		#Buffer di appoggio per leggere un nomeTask
.align 2			#allineo ad un indirizzo multiplo di 4 (2^2)

#STRINGHE FUNZIONE DI STAMPA TASK:
strDelim: .asciiz "+----+-----------+-----------+-------------------+\n" 	     #Delimitatore righe
strTitle: .asciiz "| ID | PRIORITA' | NOME TASK | ESECUZ. RIMANENTI |\n"  #titolo
strGroup_1: .asciiz "| "
strGroup_2: .asciiz " |     "
strGroup_3: .asciiz "     | "
strGroup_4: .asciiz "  |        "
strGroup_5: .asciiz "         |\n"
strSpace: .asciiz " " 			#solo per esigenze di stampa
.align 2				#allineo ad un indirizzo multiplo di 4 (2^2)

#STRINGHE FUNZIONE CREA NUOVO TASK:
strCTPres: .asciiz "CREAZIONE NUOVO TASK:\n"
strInsPr:   .asciiz "Inserire la priorita' (tra 0 e 9): "
strInsNome:   .asciiz "Inserire il nome (max 8 caratteri): "
strInsExe: 	.asciiz "Inserire il numero di esecuzioni (tra 1 e 99): "

#STRINGHE ERRORI INSERIMENTI
strInsErr:    .asciiz "\tErrore nell'inserimento!!\n\n"
strInsErr2:  .asciiz "\tErrore nell'inserimento!!\nInserire uno 0 davanti ai numeri con una sola cifra!\n\n"
strReInsert: .asciiz "Inserire il valore richiesto: " #da visualizzare dopo un errore!

#STRINGHE FUNZIONE CHIEDI ID:
strID:	.asciiz "Inserire l'ID del task (0-99): "
strIDNF: .asciiz "ID non trovato! Termino il programma!!\n\n"

#STRINGHE FUNZIONE CAMBIA PRIORITA':
strCP: .asciiz "Inserire la nuova priorità (0-9): "

.text
.globl main
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
chooseOp:	#PROCEDURA CHOOSEOP: Stampa il menu e attende la scelta dell'utente
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria

	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	printMenu:
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strMenu	#indico la stringa
		syscall 			#stampo

	chooseN:
		jal readOne		#leggo un numero tra 0 e 9
		move $t0, $v0	#prendo l'intero letto -> t0=scelta fatta

		#Controllo l'ammissibilità 
		#scelta<9
		li $t1,9
		slt $t2, $t0,$t1				#t2=1 -> Scelta ammissibile
		beq $t2,$zero printChooseErr	#scelta non ammissibile
		#scelta>0
		li $t1,1
		slt $t2, $t0,$t1				#t2=1 -> Scelta non ammissibile
		bne $t2,$zero printChooseErr	#scelta non ammissibile

		#vado a capo
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strACapo	#indico la stringa
		syscall

		### Switch ###  --> nei vari case si impostano i parametri per la chiamata delle procedure
		addi $t0, $t0, -1 	# tolgo 1 da scelta perche' prima azione nella jump table (in posizione 0) corrisponde alla prima scelta del case
		sll $t1, $t0, 2		#t1= (scelta-1) * 4  -- tramite 2 shift a sinistra
		la $t2, strJAT		#prendo l'indirizzo della Jump Adress Table
		add $t2, $t2, $t1 	# sommo all'indirizzo della prima case action l'offset calcolato sopra
		lw $t0, 0($t2)   	# $t0 = indirizzo a cui devo saltare

		jr $t0 	# salto all'indirizzo calcolato


	choose_1: #Case 1: INSERIRE NUOVO TASK
		#Imposto i parametri per la chiamata:
		jal creaTask
		move $a0, $v0   #salvo ptr al task per passarlo alla funzione
		jal insertTask 	#inserisco in lista
		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_2: #Case 2: ESEGUO IL PRIMO TASK
		#Imposto i parametri per la chiamata:
		la $t0, ptrCodaLista
		lw $a0, 0($t0)	#carico l'indirizzo della coda della lista (che è il primo task da eseguire)

		jal eseguiTask	#eseguo il task

		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_3: #Case 3: ESEGUIRE TASK a SCELTA
		#Imposto i parametri per la chiamata:
		jal findID		#cerco il Task
		move $a0,$v0	#passo il ptr trovato
		jal eseguiTask	#eseguo il task scelto

		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_4: #Case 4: ELIMINA TASK a SCELTA
		#Imposto i parametri per la chiamata:
		jal findID		#cerco il Task
		move $a0,$v0	#passo il ptr trovato
		jal extractTask #estraggo dalla lista
		move $a0,$v0	#passo il ptr restituito
		jal collectGarbage	#metto nel GarbageCollector

		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_5: #Case 5: MODIFICA PRIORITA' TASK a SCELTA
		#Imposto i parametri per la chiamata:
		jal findID		#cerco il Task
		move $a0,$v0	#passo il ptr trovato
		jal changePriority #cambio la priorità 

		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_6: #Case 6: CAMBIA LA POLITICA
		#Imposto i parametri per la chiamata:
		jal changePolicy    #chiamo ilmetodo per cambiare la policy

		jal printTable	#stampo la lista dei task
		j printMenu		#terminata una procedura torno al menu

	choose_7: #Case 7: ESCE DAL PROGRAMMA
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strExit	#indico la stringa
		syscall				#stampo
		j returnToMain		#torno alla main

	choose_8: #Case 8: CARICA TASK DA FILE (DEBUG)
		jal loadTasks	#chiamo la procedura per la creazione di una nuova lista di task da file
		jal printTable	#stampo i task appena creati
		j printMenu		#terminate le procedure torno al menu

	printChooseErr:
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strErr		#indico la stringa
		syscall 			#stampo

		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strChoose	#indico la stringa
		syscall 			#stampo

		j chooseN 		#vado alla scelta del numero

	returnToMain:
	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
creaTask:	#PROCEDURA CREATASK: crea un record e lo riempie con i dati inseriti dall'utente
	#restituisce:
	#v0= ptr al nuovo task creato
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -8	#alloca memoria

	sw $s0, 4($sp) 		#salva s0 nello stack
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	la $t0, ptrTestaGarbage
	lw $t0, 0($t0)	#carico la testa del garbage
	beq $t0,$zero, creaNuovo	#se il Garbage è vuoto creo un nuovo record
	#altrimenti riciclo:
	jal recycleGarbage		#vado alla procedura di riciclo
	j riempiTask			#vado a riempire il record

	creaNuovo:#Creo un nuovo record
		li $v0, 9					#codice chiamata
		li $a0, 16					#byte da allocare
		syscall                     # chiamata sbrk: restituisce un blocco di a0 byte, puntato da v0: il nuovo record

	riempiTask:	#riempio il task con i valori dati dall'utente
		move $s0,$v0	#salvo il ptr al nuovo record -> s0= ptr record

	#stampa intro:
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strCTPres	#indico la stringa
	syscall 			#stampo

	setId:#Assegno l'ID:
		la $t1,id_max	#prendo l'indirizzo
		lb $t0,0($t1)	#carico l'ultimo id assegnato
		addi $t0,$t0,1	#t0= nuovo ID
		sb $t0, 0($s0)	#Metto l'id nel record
		sb $t0,0($t1) 	#salvo in memoria il nuovo ID_max

	setPriority:#Assegno la priorità :
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsPr	#indico la stringa
		syscall 			#stampo

		jal readOne				#leggo il valore
		move $t0,$v0 			#t0= valore letto e controllato

		sb $t0, 1($s0)			#salvo nel record

	setName: #prendo il nome del task
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsNome	#indico la stringa
		syscall 			#stampo

		# legge la scelta
		li $v0, 8				#codice leggi stringa(necessita di un buffer di appoggio)
		#calcolo l'offset per l'inserimento nel record:
		addi $a0,$s0,2			#a0= ptr stringa
		li $a1, 9				#char da leggere + 1 (per il fine stringa)
		syscall					#leggo

		#inizializzo
		addi $t0, $s0,2			#t0= ptr stringa
		move $t1,$zero			#t1= contatore caratteri
		#t2= char estratto
		li $t3,9				#t3= count_max
		li $t5,' '				#t5= space

		contaChar:
			lb $t2, 0($t0)			 	#estraggo un char
			beq $t2,$zero, removeRC 	#se ho trovato 0 la stringa è finita, aggiusto gli spazi
			#altrimenti:
			addi $t0, $t0, 1		 	#muovo il puntatore
			addi $t1, $t1, 1		 	#incremento il contatore
			j contaChar					#leggo un nuovo char

		removeRC:	#toglie il carattere \n
			beq $t1,$t3, insertNewLine  #se counter==8 allora la stringa è piena e posso uscire
			#se la parola è meno di 8 char è stato inserito sicuramente uno \n, quindi:
			addi $t0, $t0, -1			#torno indietro di 1 col puntatore
			addi $t1, $t1, -1			#decremento il contatore
			#ora posso sovrascrivere lo \n dal ciclo di inserimento degli spazi:

		setSpaces: #aggiusta gli spazi alla fine della parola per farla diventare di 8 char precisi
			beq $t1,$t3, setEoS		#se counter==8 allora la stringa è piena e posso uscire
			sb $t5, 0($t0)			#metto uno spazio nel buffer di appoggio
			addi $t0, $t0, 1		#muovo il puntatore
			addi $t1, $t1, 1		#incremento il contatore
			j setSpaces

		insertNewLine:#vado a capo (solo per la grafica)
			li	$v0, 4				#codice per stampare una stringa
			la	$a0, strACapo		#indico la stringa
			syscall 				#stampo

		setEoS: 	#metto il carattere di fine stringa:
			sb $zero, 10($s0)
			#proseguo:

	setExe:
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsExe	#indico la stringa
		syscall 			#stampo

		jal readTwo				#leggo il valore
		move $t0,$v0 			#t0= valore letto e controllato

		sb $t0, 11($s0)			#salvo nel record
		#A questo punto ho finito, dando per scontato che il campo ptr.next sia già  a zero
		j exitCT


	insErr: #errore nell'inserimento
		#stampo messaggio di errore
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsErr	#indico la stringa
		syscall 			#stampo

		#ricomincio l'inserimento
		j setPriority

	exitCT: #esco dalla procedura
		move $v0,$s0	#return il ptr al record creato

	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra
	lw $s0, 4($sp) 		#riprendo s0

	addi $sp, $sp, 8	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
readOne:	#PROCEDURA DI APPOGGIO: legge una carattere da tastiera, lo controlla e restituisce un intero
	#return:
	#v0= intero letto (0-9)

	readOChoice:
		# legge la scelta
		li $v0, 8				#codice leggi stringa(necessita di un buffer di appoggio)
		la $a0, chooseBufferOne	#indirizzo "buffer" di appoggio
		li $a1, 2				#char da leggere + 1 (per il fine stringa)
		syscall					#leggo

		la $t0,chooseBufferOne
		lb $t0, 0($t0)	    	#t0= scelta

		#controllo che sia ammissibile
		# scelta<10
		li $t1,':'						# i : sono il carattere ascii succesivo al 9
		slt $t2, $t0,$t1				#t2=1 -> Scelta ammissibile
		beq $t2,$zero insOErr			#scelta non ammissibile
		#scelta>-1
		li $t1,'0'
		slt $t2, $t0,$t1				#t2=1 -> Scelta non ammissibile
		bne $t2,$zero insOErr			#scelta non ammissibile
		#a questo punto la scelta è un numero, quindi lo converto in int:
		addi $t0, $t0, -48		#sottraggo 48 dal codice ascii per ottenere l'int
		j returnRO

	insOErr: #errore nell'inserimento
		#stampo messaggio di errore
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsErr	#indico la stringa
		syscall 			#stampo

		#stampo messaggio per inserimento
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strReInsert#indico la stringa
		syscall 			#stampo

		#ricomincio l'inserimento
		j readOChoice

	returnRO:
		#vado a capo
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strACapo	#indico la stringa
		syscall

		move $v0,$t0	#parametro di ritorno
		jr $ra 			#torno al chiamante
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
readTwo: 	#PROCEDURA DI APPOGGIO: legge 2 caratteri da tastiera, li controlla e restituisce un intero
	#return:
	#v0= intero letto (00-99)

	readOChoice2:
		# legge la scelta
		li $v0, 8				#codice leggi stringa(necessita di un buffer di appoggio)
		la $a0, chooseBufferTwo	#indirizzo "buffer" di appoggio
		li $a1, 3				#char da leggere + 1 (per il fine stringa)
		syscall					#leggo

		la $t3, chooseBufferTwo #Prendo la scelta -> t3= ptrChar
		#t0= char estratto
		#t1= char di controllo
		move $t2,$zero			#t2= zero (per la conversione)
		#t3= ptr char (per scorrere)
		li $t4,10				#t4= 10 per moltiplicare

	leggiInt:
		lb $t0, 0($t3)			#Prendo un char
		beq $t0,$zero,returnRT	#se è zero ho finito
		addi $t3,$t3,1			#incremento il ptr
		#controllo che sia ammissibile
		# scelta<10
		li $t1,':'						# i : sono il carattere ascii succesivo al 9
		slt $t1, $t0,$t1				#t1=1 -> Scelta ammissibile
		beq $t1,$zero insErr2	#scelta non ammissibile
		#scelta>-1
		li $t1,'0'
		slt $t1, $t0,$t1				#t1=1 -> Scelta non ammissibile
		bne $t1,$zero insErr2	#scelta non ammissibile
		#a questo punto la scelta è un numero, quindi lo converto in int:
		addi $t0, $t0, -48		#sottraggo 48 dal codice ascii per ottenere l'int
		#ora per ridare le decine:
		mul $t2, $t2, $t4 		#t2= t2*10
		add $t2, $t2, $t0		#t2= t2*10 + t0
		j leggiInt				#torno all'inizio

	insErr2: #errore nell'inserimento
		#stampo messaggio di errore
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strInsErr2	#indico la stringa
		syscall 			#stampo

		#stampo messaggio per inserimento
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strReInsert#indico la stringa
		syscall 			#stampo

		#ricomincio l'inserimento
		j readOChoice2

	returnRT:
		#vado a capo
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strACapo	#indico la stringa
		syscall

		move $v0,$t2	#parametro di ritorno
		jr $ra 			#torno al chiamante
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
recycleGarbage: #PROCEDURA RECYCLE GARBAGE: restituisce il ptr ad un record da riciclare
	#return:
	#v0= ptr record da riciclare

	#prendo il ptr alla lista Garbage
	la $t1, ptrTestaGarbage
	lw $t8, 0($t1)			#t8= ptr testaGarbage

	#questa funzione viene chiamata dopo aver controllato che il garbage non è vuoto
	#quindi posso sicuramente estrarre un record!
	#ESTRAZIONE IN TESTA
	move $v0,$t8		#copio il ptr da estrarre
	lw $t8, 12($t8)		#Carico la nuova testa
	sw $zero, 12($v0)	#Scollego il record da estrarre dal Garbage
	sw $t8, 0($t1)		#salvo la nuova testa

	#torno al chiamante
	jr $ra
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insertTask: 	#inserisce task in lista in modo ordinato
	#input $a0 (ptr al task), output none
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria

	sw $s0, 0($sp)		#salva s0 nello stack
	#########################################################

	la $t1, ptrTestaLista
	lw $t8, 0($t1)              #t8 = testa

	beq $zero, $t8, emptyInsert #gestisco primo inserimento

	la $t1, ptrCodaLista
	lw $t9, 0($t1)              #t9 = coda

	la $t1, policy
	lb $t0, 0($t1)              #t0 = policy

	li $t3, 'a'          		#metto il carattere di priorita in $t3
	bne $t0, $t3, executions    #confronto per cambio inserimento in base alla policy corrente


	priority:   #ordino per priorita (decrescente)
		lb $t5, 1($a0)          #t5 = priorita task

		move $t6, $t8               #copio indirizzo testa per esigenze di iterazione
		move $t1, $zero
		move $t1, $t6               #copio indirizzo testa per esigenze di iterazione

	loopInsertP:
		beq $t6, $zero, insert    #gestisco fine ciclo e inserimento in coda
		lb $t2, 1($t6)          #carico la priorita di un task dalla lista
		beq $t5, $t2, IDControl #con priorita uguale controllo l'ID
		
		slt $t3, $t5, $t2
		beq $t3, $zero, insert  #t5 > t2 ==> inserimento
		
		beq $t1, $t6, incremento_t6P
		move $t1, $t6           #incremento t1

		incremento_t6P:          #incremento t6
			lw $t6, 12($t6)

		j loopInsertP


	executions: #ordino per esecuzioni (crescente)
		lb $t5, 11($a0)             #t5 = esecuzioni task

		move $t6, $t8               #copio indirizzo testa per esigenze di iterazione
		move $t1, $zero
		move $t1, $t6               #copio indirizzo testa per esigenze di iterazione

	loopInsertE:
		beq $t6, $zero, insert    #gestisco fine ciclo e inserimento in coda
		lb $t2, 11($t6)          #carico le esecuzioni di un task dalla lista
		beq $t5, $t2, IDControl #con priorita uguale controllo l'ID

		slt $t3, $t2, $t5
		beq $t3, $zero, insert  #t5 > t2 ==> inserimento

		beq $t1, $t6, incremento_t6E
		move $t1, $t6           #incremento t1

		incremento_t6E:          #incremento t6
			lw $t6, 12($t6)

		j loopInsertE



	IDControl:
		lb $t4, 0($a0)          #t4 = ID task da inserire
		lb $t3, 0($t6)          #t3 = ID task lista

		slt $t7, $t4, $t3
		beq $t7, $zero, insert  #t4 > t3 ==> insert

		beq $t1, $t6, incremento_t6I
		move $t1, $t6           #altrimenti incremento

		incremento_t6I:
			lw $t6, 12($t6)

		li $t3, 'a'                #metto il carattere di priorita in $t3
		beq $t0, $t3, loopInsertP  #se non trovo inserimento dopo aver incrementato scelgo quale loop richiamare in base alla policy
		j loopInsertE 			   #se non è per priorità  è sicuramente per exe

	headInsert:
		sw $t6, 12($a0)         #collego task alla lista
		la $t1, ptrTestaLista
		sw $a0, 0($t1)          #aggiorno la testa

		j returnInsert

	tailInsert:
		la $t1, ptrCodaLista
		sw $a0, 0($t1)          #aggiorno la coda
		sw $zero, 12($a0)       # metto il ptr finale a null

		j returnInsert

	insert:
		beq $t1, $t6, headInsert    #aggiorno testa se necessario

		sw $a0, 12($t1)         #collego task alla lista
		sw $t6, 12($a0)         #collego il resto della lista al task

		beq $t6, $zero, tailInsert    #aggiorno coda se necessario

		j returnInsert


	emptyInsert:
		sw $a0, 0($t1)      #aggiorno testa

		la $t1, ptrCodaLista    #carico la coda
		sw $a0, 0($t1)      #aggiorno coda

	returnInsert:
	#########################################################
	#POP nello Stack:
	lw $s0, 0($sp)		#riprendo s0

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
changePolicy:   #cambia la politica di scheduling attuale e riordina la lista rendendola conforme alla nuova politica
	# input: none; output: none
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -8	#alloca memoria

	sw $s0, 4($sp)		#salva s0
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	la  $t0, policy     #carico la policy per confronto
	lb $t2, 0($t0) 		#t2= policy
	li $t1, 'a'       #policy priorita

	bne $t2, $t1, policyP  #policy attuale != priorita ==> priorita
	#altrimenti esecuzioni
	PolicyE:               #cambio policy
		li $t1, 'b'
		sb $t1, 0($t0)
		j reOrder

	policyP:                #cambio policy
		li $t1, 'a'
		sb $t1, 0($t0)

	reOrder:
		la $t0, ptrTestaLista
		lw $s0, 0($t0)          #salvo il valore della testa prima di eliminarla   -> s0=ptr_vecchia_testa
		sw $zero, 0($t0)        #elimino la testa della lista

		la $t0, ptrCodaLista    #non salvo la vecchia coda poiche non mi serve
		sw $zero, 0($t0)        #elimino la coda della lista

		loopR:
			move $a0, $s0       #passo l'indirizzo del task da reinserire alla nuova lista
			lw $s0, 12($s0)		#scorro il ptr
			sw $zero, 12($a0)   #stacco il record dalla vecchia lista
			jal insertTask      #inserisco il task ordinato secondo la politica di scheduling nella nuova lista
			beq $s0, $zero, returnCP #se la testa e' uguale al ptr nullo esco
			j loopR             #itero fino a fine lista

	returnCP:
	#########################################################
	#POP nello Stack:
	lw $s0, 4($sp)		#ripristino s0
	lw $ra, 0($sp)		#ripristino ra

	addi $sp, $sp, 8	#dealloco

	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
collectGarbage: #PROCEDURA COLLECT GARBAGE: inserisce un task nel Garbage
	#a0= ptr Record da inserire
	#return: void

	#inserimento in testa
	la $t1, ptrTestaGarbage #carico l'indirizzo del garbage
	lw $t8, 0($t1)			#t8= testa del garbage
	sw $t8, 12($a0)			#collego il record da inserire al garbage
	sw $a0, 0($t1)			#salvo la nuova testa

	#torno al chiamante
	jr $ra
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
eseguiTask:		#PROCEDURA ESEGUI TASK: esegue il task assegnato.
	#a0= ptr task da eseguire
	#return void
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria

	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	#Controllo il nÂ° di esecuzioni:
	lb $t0, 11($a0)		#t0= nÂ° di esecuzioni
	li $t1, 2			#t1= char di controllo
	slt $t2, $t0,$t1	#t2=1 se t0==1
	bne $t2,$zero, lastExe	#se è l'ultima esecuzione vado a rimuovere il task dalla lista
	#altrimenti:
	addi $t0,$t0,-1		#decremento il nÂ° di esecuzioni
	sb $t0, 11($a0)		#salvo nel record
	#Controllo la Policy:
	la $t1, policy 	  	#prendo l'indirizzo
	lb $t2, 0($t1)		#t2= policy
	li $t1, 'a'			#t1= char di controllo
	beq $t2,$t1, returnET	#Se la politica è su Priorità  allora ho finito
	#Altrimenti la politica è sul numero di esecuzioni, quindi estraggo e reinserisco il task

	reInserisciTask:
		#ho già  il ptr del Task in a0, quindi
		#Per rimettere il task in posizione ordinata:
		jal extractTask	#tolgo il task dalla lista

		move $a0,$v0	#riprendo il ptr del task
		jal insertTask  #inserisco il task in modo ordinato in lista

		j returnET	#ho finito


	lastExe:	#Ultima esecuzione di un task
		#ho già  il ptr del Task in a0, quindi
		jal extractTask 	#estraggo il task dalla lista

		move $a0,$v0	#riprendo il ptr del task
		jal collectGarbage	#lo metto nel garbage
		#ho finito:

	returnET: #ritorno al chiamante
	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extractTask:    #PROCEDURA ESTRAZIONE TASK
	#input: $a0 (indirizzo del task da estrarre); 
	#return: $v0 (indirizzo dell'elemento estratto)

	move $t0, $a0               #salvo in $t0 l'indirizzo del task da estrarre preso in ingresso tramite $a0

	la $t6, ptrTestaLista           #prendo indirizzo head ->t6= indirizzo salvataggio testa
	lw $t8, 0($t6)                  #carico indirizzo head in $t8
	move $t2, $t8					#t2= indirizzo testa (per scorrimento)

	beq $t0, $t2, headExtraction    #gestisco estrazione testa per aggiornare il ptr (Estrazione in Testa)

	la $t7, ptrCodaLista            #prendo indirizzo tail ->t7= indirizzo salvataggio coda
	lw $t9, 0($t7)                  #carico indirizzo tail in $t9

	beq $t8, $t9, lastExtraction    #gestisco a parte l'estrazione dell'ultimo elemento della  lista

	move $t5, $t2                   #salvo indirizzo della testa per mantenere sempre un ptr all'elemento attuale

	loopExtract:
		lw $t4, 12($t2)             #carico in $t4 indirizzo del successivo

		beq $t0, $t4, extraction   	 #trovato il task
		beq $t2, $t5, incremento_t2E #utile per primo incremento(che deve essere sfalsato)
		move $t5, $t2

		incremento_t2E:
			move $t2, $t4               #carico indirizzo del successivo dal ptr attuale

		j loopExtract               #itero

	headExtraction:
		lw $t4, 12($t2)     #carico indirizzo del successivo alla testa
		sw $t4, 0($t6)        #sostituisco la testa
		j returnE

	tailExtraction:
		sw $t5, 0($t7)        #sostituisco la coda
		j returnE

	lastExtraction:
		sw $zero, 0($t6)    #azzero ptr testa
		sw $zero, 0($t7)    #azzero ptr coda
		j returnE

	extraction:
		move $t5, $t2       #scorro la lista di 1 per procedere con l'estrazione
		move $t2, $t4       #scorro la lista di 1 per procedere con l'estrazione

		lw $t4, 12($t2)     #carico indirizzo del successivo all'estratto
		sw $t4, 12($t5)     #estraggo elemento dalla lista scollegandolo

		beq $t0, $t9, tailExtraction    #gestisco estrazione coda per aggiornare il ptr

	returnE:
		move $v0, $t0       #ritorno indirizzo dell'eliminato
		jr $ra               #ritorno la funzione
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
findID:     #PROCEDURA di appoggio per trovare processo con ID selezionato
	#return: v0=ptr task con ID selezionato
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria

	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	requestID:
		#Richiesta:
		li $v0, 4       #codice stampa stringa
		la $a0, strID   #stringa  da stampare
		syscall         #stampo

		jal readTwo				#leggo il valore
		move $t0,$v0 			#t0= valore letto e controllato

	gotID:  ##Cerco l'ID## —>t0=ID
		#Se l'ID è maggiore dell'Id massimo -> sicuramente non c'è
		la $t1, id_max              #carico l'indirizzo
		lb $t2, 0($t1)              #carico l'ID max
		slt $t1, $t2, $t0           #t1=1 se ID_max<ID
		bne $t1,$zero,idNotFound    #se ID è maggiore di ID_max -> non trovato

		#Cerco nella Lista
		la $t1, ptrTestaLista       #prendo indirizzo head
		lw $t8, 0($t1)              #carico indirizzo head in $t8

		lb $t4, 0($t8)              #metto in $t4 l'ID dell'elemento
		move $t2, $t8 				#t2= ptrID
		beq $t4, $t0, returnFI		#se l'ID è nel primo elemento ho finito
		la $t1, ptrCodaLista        #prendo indirizzo tail
		lw $t9, 0($t1)              #carico indirizzo tail in $t9

		lb $t4, 0($t9)              #metto in $t4 l'ID dell'elemento
		move $t2, $t9				#copio la coda (nel caso di uguaglianza la reestituisco)
		beq $t4, $t0, returnFI		#se l'ID è nel task in coda ho finito

		move $t6, $t4               #salvo id coda per fine loop

		#SCORRO LA LISTA E CERCO L'ID
		move $t2, $t8				#t2= ptrTask (per scorrere)

		loopFind:
			lb $t4, 0($t2)              #metto in $t4 l'ID dell'elemento

			beq $t4, $t0, returnFI      #confronto per uscire
			beq $t4, $t6, idNotFound    #confronto per elemento non trovato

			lw $t2, 12($t2)             #incremento $t2

			j loopFind                  #itero


	idNotFound: ##elemento non trovato##
		#stampo messaggio di errore
		li  $v0, 4          #codice per stampare una stringa
		la  $a0, strIDNF	#indico la stringa
		syscall             #stampo
		jal killAll

	returnFI:  ###RETURN
		move $v0, $t2               #salva l'indirizzo all'elemento scelto per restituirlo
	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
changePriority: #PROCEDURA CHANGE PRIORITY: cambia la priorità  di un Task
	#a0= ptr al task
	#return: void

	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -8	#alloca memoria

	sw $s0, 4($sp) 		#salva s0 nello stack
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################
	move $s0,$a0		#Salvo il ptr -> s0=ptr task

	#Stampo la richiesta
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strCP		#indico la stringa
	syscall 			#stampo

	jal readOne			#leggo il valore
	move $t0,$v0 		#t0= valore letto e controllato

	sb $t0, 1($s0)		#salvo nel record

	policyControl:
		#Controllo la Policy:
		la $t1, policy 	  	#prendo l'indirizzo
		lb $t2, 0($t1)		#t2= policy
		li $t1, 'a'			#t1= char di controllo
		bne $t2,$t1, returnCT	#Se la politica è su nEsecuzioni allora ho finito
		#Altrimenti la politica è su Priorità , quindi:
		#-Poichè per vedere la priorità  del precedente dovrei comunque scorrere tutta la lista,
		#-Estraggo e reinserisco il Task anche se era già  ordinato:

		move $a0,$s0	#passo il ptr al task
		#Per rimettere il task in posizione ordinata:
		jal extractTask	#tolgo il task dalla lista
		move $a0,$v0	#riprendo il ptr del task
		jal insertTask  #inserisco il task in modo ordinato in lista

	returnCT: #ho finito
	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra
	lw $s0, 4($sp) 		#riprendo s0

	addi $sp, $sp, 8	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
killAll:	#PROCEDURA DI EMERGENZA
	li $v0, 10
	syscall
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
printTable:	#PROCEDURA STAMPA TASKS
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -8	#alloca memoria

	sw $s0, 4($sp)		#salva s0
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################

	printTitle: #Stampo l'intestazione della tabella:
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strDelim	#indico la stringa
		syscall 			#stampo

		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strTitle	#indico la stringa
		syscall 			#stampo

		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strDelim	#indico la stringa
		syscall 			#stampo

	initPT: #inizializzo
		la $t1,ptrTestaLista	#prendo l'indirizzo alla testa
		lw $s0, 0($t1)			#s0= ptrTestaLista

	loopList: #loop di scorrimento della lista dei task
		beq $s0, $zero, exitPT	# se  s0 == 0 si e' raggiunta la fine della lista e si esce
		move $a0,$s0			#passo il ptr del record
		jal printTask			#stampo il record
		lw $s0, 12($s0)			#Scorro il ptr all'elemento successivo
		#stampo un delimitatore
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strDelim	#indico la stringa
		syscall 			#stampo
		j loopList	#ricomincio il loop di stampa

	exitPT: #Ho finito di stampare la tabella
		# Stampo la policy attuale:
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strPolicy	#indico la stringa
		syscall 			#stampo

		#Stampo un a capo per staccarmi dalla tabella
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strACapo	#indico la stringa
		syscall 			#stampo

		#Stampo un a capo per staccarmi dalla tabella
		li	$v0, 4			#codice per stampare una stringa
		la	$a0, strACapo	#indico la stringa
		syscall 			#stampo

	#ora posso tornare al chiamante:
	#########################################################
	#POP nello Stack:
	lw $s0, 4($sp)		#ripristino s0
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 8	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
printTask:	#PROCEDURA STAMPA UN SINGOLO TASK (ovvero una riga della tabella)
	#a0= ptrRecord
	#return: void

	#inizializzo
	move $t8,$a0	#copio l'indirizzo di testa (altrimenti lo perdo nelle syscall)
	li $t5,10		#t5= int di confronto


	#Stampo gruppo_1
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strGroup_1	#indico la stringa
	syscall

	#Stampo ID:
	lb $t0, 0($t8)		#carico l'ID (int)
	slt $t1, $t0,$t5	#t1=1 if ID<10
	beq $t1,$zero, nextP #se è >10 lo stampo senza spazi davanti
	#stampo uno spazio:
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strSpace	#indico la stringa
	syscall
	
	#proseguo nella stampa dell'ID
	nextP:
	li	$v0, 1			#codice per stampare un intero
	move $a0,$t0		#carico l'intero da stampare
	syscall

	#Stampo gruppo_2
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strGroup_2	#indico la stringa
	syscall

	#Stampo PRIORITA':
	li	$v0, 1			#codice per stampare un intero
	lb $a0, 1($t8)		#carico l'intero da stampare
	syscall

	#Stampo gruppo_3
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strGroup_3	#indico la stringa
	syscall

	#stampo NOMETASK:
	li	$v0, 4			#codice per stampare una stringa
	addi $a0, $t8,2		#posizione del nomeTask nel record (quindi a0= base+offset)
	syscall

	#Stampo gruppo_4
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strGroup_4	#indico la stringa
	syscall

	#Stampo ESECUZIONI:
	lb $t0, 11($t8)		  #carico il num di esecuzioni (int)
	slt $t1, $t0,$t5	  #t1=1 if num<10
	beq $t1,$zero, nextPr #se è >10 lo stampo senza spazi davanti
	#stampo uno spazio:
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strSpace	#indico la stringa
	syscall
	#proseguo nella stampa del num di esecuzioni
	nextPr:
	li	$v0, 1			#codice per stampare un intero
	move $a0,$t0		#carico l'intero da stampare
	syscall

	#Stampo gruppo_5
	li	$v0, 4			#codice per stampare una stringa
	la	$a0, strGroup_5	#indico la stringa
	syscall

	#Ho finito di stampare, quindi torno al chiamante
	jr $ra		 	# torno al chiamante
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
loadTasks:	#PROCEDURA CARICA TASKS DA FILE
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -8	#alloca memoria

	sw $s0, 4($sp)		#salva s0
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################
	# Open File
	open:
		li	$v0, 13		 # Open File Syscall
		la	$a0, strFile # Load File Name
		li	$a1, 0		 # Read-only Flag
		li	$a2, 0		 # (ignored)
		syscall
		move $t6, $v0	# Save File Descriptor
		blt	$v0, 0, err	# Goto Error

	# Read Data
	read:
		li	$v0, 14			# Read File Syscall
		move $a0, $t6		# Load File Descriptor
		la	$a1, taskBuffer	# Load Buffer Address
		li	$a2, 184		# Buffer Size
		syscall

	# Close File
	close:
		li	$v0, 16			# Close File Syscall
		move $a0, $t6		# Load File Descriptor
		syscall
		j	initLT		# Vado a manipolare i dati

	# Error
	err:
		li	$v0, 4		# Print String Syscall
		la	$a0, strFnF	# Load Error String
		syscall


	initLT: #inizializzo
		move $s0,$zero		#s0= testa della lista dei task
		la $t0, taskBuffer	#t0= ptr al buffer da cui estraggo i task
		#t1= char estratto dal buffer per controllo

	loopLT: #loop di caricamento dei task dal buffer
		lb $t1, 0($t0)		    	#estraggo il primo char dal buffer
		beq $t1,$zero, exitLT		#se è uguale a 0 ho finito di estrarre
		#altrimenti:
		#Creo un nuovo record
		li $v0, 9					#codice chiamata
		li $a0, 16					#byte da allocare
		syscall                     # chiamata sbrk: restituisce un blocco di a0 byte, puntato da v0: il nuovo record

		beq $s0,$zero, salvaCoda	#se la testa è uguale a zero, salvo anche la coda (basta al primo inserimento, perchè inserisco in testa)
	goOn: #proseguo
		#INSERIMENTO IN TESTA:
		sw $s0,12($v0)				#il nuovo record viene inserito in testa
		move $s0, $v0				#v0 è la nuova testa della lista


	# vengono riempiti i 4 campi del nuovo record:
	#leggo l'ID:
	move $a0, $t0		#passo il ptr al buffer
	jal readInt			#chiamo la procedura per leggere l'intero
	sb $v1, 0($s0)		#lo salvo nel record
	la $t7, id_max 		#prendo l'id_max
	lb $t6, 0($t7)		#carico l'id_max
	slt $t0,$t6,$v1		#t0=1 se id_max<id_new
	beq $t0,$zero,nextLT	#t0=0 -> id_max non è da aggiornare
	sb $v1,0($t7)		#salvo il nuovo id_max

	nextLT:
	#leggo la priorità :
	move $a0, $v0		#passo il ptr al buffer
	jal readInt			#chiamo la procedura per leggere l'intero
	sb $v1, 1($s0)		#lo salvo nel record
	#leggo il Nome del Task:
	move $a0, $v0		#passo il ptr al buffer
	jal readString		#chiamo la procedura per leggere la Stringa

	#salvo char by char nel record:
	la $t0, strBuffer	#poichè la funzione salva la parola in un buffer di appoggio-> t0= ptr buffer
	addi $t1, $s0,2		#posizione di partenza per l'inserimento nel record 		-> t1= ptr record
	#-> t2= char estratto
	copyLoop:
	lb $t2, 0($t0)	#estraggo un char dal buffer
	sb $t2, 0($t1)  #salvo nel record
	beq $t2,$zero, nextField	#se il char estratto è quello di fine stringa ho finito e passo al campo successivo
	addi $t0,$t0,1	#incremento il ptr al buffer
	addi $t1,$t1,1  #incremento il ptr al record
	j copyLoop

	nextField:
	#leggo le esecuzioni rimanenti
	move $a0, $v0		#passo il ptr al buffer
	jal readInt			#chiamo la procedura per leggere l'intero
	sb $v1, 11($s0)		#lo salvo nel record

	#Ho finito di riempire il record, quindi ricomincio il loop:
	move $t0, $v0	#copio il ptr al buffer
	j loopLT

	salvaCoda:#salvo la coda
	la $t1, ptrCodaLista #inidirizzo salvataggio coda
	sw $v0, 0($t1)		 #salvo la coda
	j goOn				#vado all'inserimento dati

	exitLT:	#ho finito di creare la lista, salvo in memoria il ptr alla testa:
	la $t0, ptrTestaLista	#carico l'indirizzo
	sw $s0, 0($t0)		#salvo in memoria

	#aggiorno ID_max:
	move $t0, $s0	#t0= ptr scorrimento
	#t1= char di controll
	#t2= id estratto
	li $t3,0		# t3= id_max

	loopIDmax:
	beq $t0, $zero, saveIDmax	#finita la lista salvo id max
	lb $t2, 0($t0)				#estraggo l'id
	slt $t1, $t3,$t2
	bne $t1,$zero, incr_id		#se t3>t2 salvo il nuovo id_max
	lw $t0, 12($t0)				#incremento il ptr
	j loopIDmax

	incr_id:
	move $t3,$t2 	#copio il nuovo max
	lw $t0, 12($t0)				#incremento il ptr
	j loopIDmax

	saveIDmax:
	la $t0, id_max #carico l'indirizzo
	sb $t3, 0($t0)	#salvo in memoria

	#ora posso tornare al chiamante:

	#########################################################
	#POP nello Stack:
	lw $s0, 4($sp)		#ripristino s0
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 8	#dealloco
	jr $ra		 		# torno al chiamante
	#########################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
readInt:	#PROCEDURA DI APPOGGIO
	#a0= ptr al buffer
	#restituisce:
	#v0= ptr aggiornato
	#v1= intero letto

	#leggo un char e lo converto in intero fino ad un carattere terminatore
	initRI:	#Leggo cià² che ho nel buffer e lo traduco in intero
	#inizializzazione
	move $t0,$a0 	#t0 punta  a un carattere della stringa
	move $t2,$zero	#t2= 0
	li $t3, ','		#t3 carattere di confronto (separatore tra numeri)
	li $t5, 10 		#per moltiplicarlo

	letturaI: #loop di estrazione dei char singoli
	lb $t1, 0($t0) 			#estraggo un char
	addi $t0, $t0, 1		#muovo il puntatore
	beq $t1, $t3, exitRI	#Numero terminato, restituisco

	#a questo punto, dando per scontato che sia un carattere numerico:
	addi $t1, $t1, -48		#sottraggo 48 dal codice ascii per ottenere l'int

	#ora per ridare le decine:
	mul $t2, $t2, $t5 		#t2= t2*10
	add $t2, $t2, $t1		#t2= t2*10 + t1

	#ricomincio a leggere
	j letturaI

	exitRI: #esco dal programma
	move $v0,$t0	#ptr aggiornato (dopo il terminatore del numero)
	move $v1,$t2	#numero letto
	jr $ra		 	# torno al chiamante
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	readString:	#PROCEDURA DI APPOGGIO
	#a0= ptr al buffer
	#restituisce:
	#v0= ptr aggiornato

	initRS:	#inizializzazione
	move $t0,$a0	#t0=ptr
	la $t5, strBuffer #t5= ptr buffer di appoggio
	move $t1,$zero	#t1= char counter
	#t2= char estratto
	li $t3,' '		#t3= spazio -> da caricare in memoria
	li $t4,','		#t4= char di confronto fine stringa
	li $t6,8		#t6= n di confronto per il counter

	letturaS: #loop di estrazione dei vari char
	lb $t2, 0($t0)			 	#estraggo un char
	addi $t0, $t0, 1		 	#muovo il puntatore
	beq $t2,$t4, aggiustaSpazi	#se ho trovato una virgola la stringa è finita, aggiusto gli spazi
	#altrimenti:
	addi $t1, $t1, 1		 	#incremento il contatore
	sb $t2, 0($t5)				#salvo il char nel buffer di appoggio
	addi $t5, $t5, 1		 	#muovo il puntatore
	j letturaS					#leggo un nuovo char

	aggiustaSpazi: #aggiusta gli spazi alla fine della parola per farla diventare di 8 char precisi
	beq $t1,$t6, exitRS		#se counter==8 allora la stringa è piena e posso uscire
	sb $t3, 0($t5)			#metto uno spazio nel buffer di appoggio
	addi $t5, $t5, 1		#muovo il puntatore
	addi $t1, $t1, 1		#incremento il contatore
	j aggiustaSpazi

	exitRS:
	move $t1,$zero
	sb $t1, 0($t5)			#metto il char di fine stringa nel buffer di appoggio

	move $v0,$t0		#restituisco il ptr aggiornato
	jr $ra 				# torno al chiamante
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#MAIN
#=========================================================
main:
	#########################################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria
	sw $ra, 0($sp)		#salva ra nello stack
	#########################################################
	#inizializzo lista
	la $t1, ptrTestaLista
	sw $zero, 0($t1)
	la $t1, ptrCodaLista
	sw $zero, 0($t1)

	jal chooseOp #lancio la procedura

	#########################################################
	#POP nello Stack:
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante (exeption handler)
	#########################################################
#=========================================================
