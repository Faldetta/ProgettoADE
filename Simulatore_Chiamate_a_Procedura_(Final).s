#SIMULATORE DI CHIAMATE A PROCEDURA

.data
buffer: .space 152 #alloco i 150 byte necessari alla lettura della stringa (2 in più per l'allineamento)
#STRINGHE ERRORI:
fnf:	.ascii  "The file was not found: "
file:	.asciiz	"chiamate.txt"
errFunc: .asciiz "Errore di sintassi nella Stringa di comando!\n\nProgramma Terminato"
divByZ: .asciiz "Errore! Divisione per Zero!\n\nProgramma Terminato"
#STRINGHE PER LE FUNZIONI DI STAMPA:
ind: .asciiz "        " 	#indentazione
count_ind:	.word 0			#counter indentazione
freccia_in: .asciiz "-->"
freccia_out: .asciiz "<--"
a_capo:	.asciiz"\n"			#per stampare a capo
strSomma: .asciiz "somma"
strSottr: .asciiz "sottrazione"
strProd: .asciiz "prodotto"
strDiv: .asciiz "divisione"
strReturn: .asciiz "-return("
strClose:  .asciiz ")\n"


.text
.globl main

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
readString: #PROCEDURA RICORSIVA DI LETTURA
	#a0= ptrStringa
	#Deve ritornare:
	#v0= returnValue
	#v1= ptrStringa

	########################################################	
	#parte di allocazione dello stack
	#PUSH nello Stack
	#mettere in memoria s0,s1,s2,ra
	addi $sp, $sp, -16	#Alloco il FrameStack

	sw $s2, 12($sp)		#salva s2
	sw $s1, 8($sp)		#salva s1
	sw $s0, 4($sp)		#salva s0
	sw $ra, 0($sp)		#salva ra nello stack(fondo)
	########################################################


	#in ingresso(quindi in a0) ho il puntatore alla Stringa
	#Carico il primo char:
	lb $t0, 0($a0)
	li $t1,'a'					 #Carattere di confronto

	slt $t2, $t0,$t1 	 		 #se t0<t1 non è un char, quindi se t2=1 -> t0=numero
	beq $t2,$zero, calcolaIDFunc #se t0 è una lettera vado ad identificare che funzione è 

	leggiInt: #Leggo il numero fino alla virgola o alla parentesi chiusa
		
			#inizializzazione:
						 	#a0 punta  a un carattere della stringa(char iniziale)
			li $t1, 0   	#t1 carattere estratto
			li $t2, 0		#t2 char convertito in intero
			li $t3, ','		#t3 carattere di confronto (separatore tra numeri) e terminatore
			li $t4, ')'		#t4 carattere di confronto (separatore tra numeri) e terminatore
			li $t5, 10 		#per moltiplicarlo
			li $t6, '-'		#t6 carattere di confronto per leggere numeri negativi
			li $t8, 1		#t8 = segno (positivo di default)

		lettura: #loop di estrazione dei char singoli
			lb $t1, 0($a0) 				#estraggo un char
			beq $t1, $t3, aggiustaPtr	#Numero terminato, salvo in memoria 									
			beq $t1, $t4, aggiustaPtr	#Numero terminato, salvo in memoria 
			beq $t1, $t6, setNegativeSign #Se trovo il - modifico il segno								
			addi $a0, $a0, 1		#muovo il puntatore (solo se ho trovato un numero, altrimenti ci pensa aggiustaPtr)
			#a questo punto, dando per scontato che sia un carattere numerico:
			addi $t1, $t1, -48		#sottraggo 48 dal codice ascii per ottenere l'int

			#ora per ridare le decine:
			mul $t2, $t2, $t5 		#t2= t2*10
			add $t2, $t2, $t1		#t2= (t2*10) + t1

			#ricomincio a leggere
			j lettura

	setNegativeSign: #se trovo il carattere '-' cambio il segno a negativo
		li $t8, -1			#segno negativo
		addi $a0, $a0, 1	#muovo il puntatore
		j lettura 			#vado a leggere il numero

	aggiustaPtr: #dopo la lettura di un intero, sposta il ptr della stringa al char successivo ad un carattere terminatore (parentesi e/o virgola)
			#in a0 ho il ptr che sta puntando al primo 'non numero'
			#quindi scorro al carattere successivo e inizio un loop di lettura finchè: o trovo un numero/lettera o arrivo a fine stringa
			addi $a0, $a0,1						#scorro di un char
			lb $t1, 0($a0)						#carico il char
			beq $t1, $zero,setReturnValues  	#se t0=0 sono arrivato in fondo alla stringa e salto alla return
			slti $t3, $t1,45					#se t0<=44 il char estratto è ) o ,
			#t3= 1 se t0 è un terminatore
			bne $t3,$zero, aggiustaPtr	#se è un terminatore scorro ancora
			#se non è un terminatore, esco e vado ad impostare i caratteri di ritorno

	setReturnValues:
			mul $t2, $t8,$t2 	#imposto il segno giusto
			move $v0, $t2 		#per ritornare il valore calcolato
			move $v1, $a0		#per ritornare il ptr alla stringa aggiornato (ovvero dopo le parentesi chiuse della funzione)
		 	#dopodichè va all'etichetta per il ritorno della funzione
		 	j toReturn



	calcolaIDFunc:
		move $s0,$a0	#sposto in s0 il ptr all'inizio della stringa per poter chiamare in sicurezza la procedura di stampa 

		#CHIAMA PROCEDURA STAMPA 
		jal printFunc

		#tornato dalla procedura di stampa:
		#s0 è il ptr al primo char della stringa
		#identifico la funzione:
		lb $t0,2($s0) #t0= 3° char

		li $t1,'m'   #t1=carattere da confrontare
		beq $t0,$t1, callSomma #SOMMA

		li $t1,'t' 
		beq $t0,$t1, callSottr #SOTTRAZIONE

		li $t1,'o' 
		beq $t0,$t1, callProd #PRODOTTO

		li $t1,'v' 
		beq $t0,$t1, callDiv #DIVISIONE

			
	#IDENTIFICAZIONE DELLE VARIE FUNZIONI:
	callSomma:
		#sposto il ptr al char dopo la parentesi aperta
		addi $s0, $s0,6		#lo metto in s0 per chiamare poi la funzione somma
		j chiamaProceduraCalcolo #vado alla chiamata della funzione

	callSottr:
		#sposto il ptr al char dopo la parentesi aperta
		addi $s0, $s0,12	#lo metto in s0 per chiamare poi la funzione sottrazione
		j chiamaProceduraCalcolo #vado alla chiamata della funzione

	callProd:
		#sposto il ptr al char dopo la parentesi aperta
		addi $s0, $s0,9	#lo metto in s0 per chiamare poi la funzione prodotto
		j chiamaProceduraCalcolo #vado alla chiamata della funzione

	callDiv:
		#sposto il ptr al char dopo la parentesi aperta
		addi $s0, $s0,10	#lo metto in s0 per chiamare poi la funzione divisione
		j chiamaProceduraCalcolo #vado alla chiamata della funzione


	chiamaProceduraCalcolo:
		move $a0, $s0		#a0= ptr stringa
		move $s1, $t1		#salvo l'ID della funzione
		move $a1, $s1		#a1=identificatore

		#CHIAMA PROCEDURA FUNZIONE 
		jal	arithmetic

		#tornato dalla procedura funzione in v0 ho il valore di ritorno
		move $s2, $v0		#salvo il valore di ritorno
		move $s0, $v1 		#salvo il ptr alla stringa ritornato


		move $a0, $s1		#a0= ID_func
		move $a1, $s2		#a1= ReturnValue
		#CHIAMA PROCEDURA STAMPA_RETURN 
		jal printReturnValue

		#tornato dalla procedura di stampa
		move $v0, $s2 	#per ritornare il valore calcolato
		move $v1, $s0	#per ritornare il ptr alla stringa aggiornato (ovvero dopo le parentesi chiuse della funzione)
		# ora vado all'etichetta per le operazioni di ritorno:

	##########################################################################
	toReturn: 
		# ripristina dallo stack i registri callee-saved che aveva salvato prima dell’esecuzione della procedura
		#POP sullo Stack:
		#prendo l'indirizzo di ritorno
		#dealloco lo stack
		lw $s2, 12($sp)		#ripristino s2
		lw $s1, 8($sp)		#ripristino s1
		lw $s0, 4($sp)		#ripristino s0
		lw $ra, 0($sp)		#ripristino ra

		addi $sp, $sp, 16	#dealloco lo stackFrame

		jr $ra          # ritorna al chiamante
	###############################################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
arithmetic: #PROCEDURA Funzione
	#a0= ptrStringa
	#a1= identificatore operazione
	#Deve ritornare:
	#v0= returnValue
	#v1= ptrStringa

	############################################################################
	#PUSH
	#devo riutilizzare ra e s0,s1
	addi $sp, $sp, -12	#alloca memoria

	sw $s1, 8($sp)		#salvo s1
	sw $s0, 4($sp)		#salvo s0
	sw $ra, 0($sp)		#salva ra nello stack	 
	#############################################################################

	move $s0, $a1	#s0=ID_funzione

	#Leggo il primo valore:
	jal readString	#Poichè ptr è già in a0 posso già chiamare la procedura

	#Salvo il valore di ritorno:
	move $s1, $v0	#s1= first_value

	#Leggo il secondo valore:
	move $a0, $v1	#stavolta devo mettere il nuovo ptr in a0
	jal readString	#chiamo la procedura

	#Eseguo l'operazione in base all'ID dato:
	li $t1,'m'   #t1=carattere da confrontare
	beq $s0,$t1, Somma #SOMMA

	li $t1,'t' 
	beq $s0,$t1, Sottr #SOTTRAZIONE

	li $t1,'o' 
	beq $s0,$t1, Prod #PRODOTTO

	
	li $t1,'v'   # quest'ultimo confronto è superfluo, ma viene lasciato per sicurezza
	beq $s0,$t1, Div #DIVISIONE	

	#operazioni:
	Somma:
		add $v0, $s1,$v0	#v0= first_value + second_value
		j done 				#salto all'etichetta per il ritorno

	Sottr:
		sub $v0, $s1,$v0	#v0= first_value - second_value
		j done 				#salto all'etichetta per il ritorno

	Prod:
		mul $v0, $s1,$v0	#v0= first_value * second_value
		j done 				#salto all'etichetta per il ritorno

	Div:
		beq $v0,$zero, divZero	#se v0=0 vado al gestore della divisione per zero
		div $v0, $s1,$v0	#v0= first_value / second_value
		j done 				#salto all'etichetta per il ritorno

	divZero:
		#stampo messaggio di errore:
			la $a0, divByZ
			li $v0, 4
			syscall	

			li $v0, 10 #esco dal programma
     	 	syscall


	#Operazioni per il ritorno al chiamante
	done:
		#poichè v0 e v1 sono già stati inseriti passo direttamente al POP dello stackFrame:
		########################################################################################
		#POP
		lw $s1, 8($sp)		#ripristino s1
		lw $s0, 4($sp)		#ripristino s0
		lw $ra, 0($sp)		#ripristino ra 

		addi $sp, $sp, 12	#dealloco lo stackFrame

		jr $ra          # ritorna al chiamante
		##########################################################################################
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
printFunc: #PROCEDURA STAMPAFUNC
	#a0= ptrStringa
	#Deve ritornare:
	#void

	move $t8, $a0		#t8= ptrStringa

	#Recupero il numero di indentazioni da fare
	la $t0, count_ind	#prendo l'indirizzo
	lw $t1, 0($t0)		#t1= count_ind
	move $t2,$t1		#uso t2 per il loop, quindi t2=t1
	
	ind_loop:
		beq $t2,$zero, count_incr #se t2=0 ho finito il ciclo
		#Stampo una indentazione:
		li	$v0, 4		#codice per stampare una stringa	
		la	$a0, ind	#indico la stringa
		syscall 		#stampo
		#decremento il counter:
		addi $t2, $t2,-1	
		j ind_loop


	count_incr:
		addi $t1, $t1,1	#incremento il counter per il prossimo ciclo
		sw $t1, 0($t0)	#salvo il counter in memoria

		#ora stampo la freccia di ingresso:
		li	$v0, 4				#codice per stampare una stringa	
		la	$a0, freccia_in 	#indico la stringa
		syscall 				#stampo

	#Cerco il punto finale a cui stampare: conto parentesi aperte e chiuse. 
	#Per tagliare la stringa, prendo il char dove devo tagliare e metto un carattere di fine stringa, dopo che ho stampato ripristino il char estratto	
	substring:
		#in t8 ho l'inizio della stringa
		#inizializzo:
		move $t0, $t8		#t0=t8 (per non perdere l'indirizzo di partenza)
		move $t1, $zero		#t1= countAperte
		move $t2, $zero		#t2= countChiuse
							#t3= char estratto
							#t4= char di confronto
		countOpen:	#conta le parentesi aperte
			addi $t0, $t0,5		#dando per scontato che il char iniziale è la stringa di una funzione, la prima parentesi aperta può trovarsi in posizione 5
			lb $t3, 0($t0)		#t3= char estratto
			li $t4,'('			#t4= Carattere di confronto

			beq $t3,$t4, addOpen	#se sono uguali ho trovato una ( quindi vado all'etichetta per contarla

			#se non ho trovato una ( non sono nella funzione somma, controllo quindi in quale sono:
			li $t4,'a'
			beq $t3,$t4, jumpSottr

			li $t4,'t'
			beq $t3,$t4, jumpProd

			li $t4,'i'			
			beq $t3,$t4, jumpDiv

			#se non ho trovato nessuna lettera corrispondente ho un errore di sintassi nel file, quindi esco dal programma!
			#stampo messaggio di errore:
			la $a0, errFunc
			li $v0, 4
			syscall	

			li $v0, 10 
     	 	syscall 		#ESCO DAL PROGRAMMA


			addOpen:	#se ho trovato la ( la conto e scorro
				addi $t1, $t1,1		#countAperte++
				addi $t0, $t0,1		#scorro
				j countClosed		#vado alla conta delle parentesi chiuse


			#se ho trovato un'altra funzione, conto la ( e scorro al char successivo
			jumpSottr:	
				addi $t1, $t1,1		#countAperte++
				addi $t0, $t0,7		#scorro
				j countClosed		#vado alla conta delle parentesi chiuse

			jumpProd:
				addi $t1, $t1,1		#countAperte++
				addi $t0, $t0,4		#scorro
				j countClosed		#vado alla conta delle parentesi chiuse

			jumpDiv:
				addi $t1, $t1,1		#countAperte++
				addi $t0, $t0,5		#scorro
				j countClosed		#vado alla conta delle parentesi chiuse

		#Reminder:
		#t0= ptr
		#t1= countAperte
		#t2= countChiuse
		#t3= char estratto
		#t4= char di confronto
		countClosed: #conta le parentesi chiuse
			lb $t3, 0($t0)		#t3= char estratto
			li $t4, ')'			#t4= confronto

			beq $t3,$t4	addClosed	#se ho trovato una parentesi chiusa vado a contarla

			li $t4, 'a'			#confronto
			slt $t5, $t3,$t4	#se t3<t4 il char è un numero (o una virgola), altrimenti è una lettera
			bne $t5,$zero, jumpNum #se t5=1 allora t3 non è una lettera
			#se non è un numero allora è una lettera:
			j countOpen		#se è una lettera è l'inizio di una funzione, quindi troverò prima una parentesi aperta

			addClosed:
				addi $t2, $t2,1		#countChiuse++
				addi $t0, $t0,1		#scorro il ptr
				beq	$t1,$t2, printSubstring	#se il numero di parentesi aperte è uguale al numero di quelle chiuse allora ho individuato la sottostringa
				j countClosed		#altrimenti continuo la conta 

			jumpNum:
				addi $t0, $t0,1		#scorro il ptr
				j countClosed		#continuo la conta



		#Ora che ho in t0 il ptr alla fine della sottostringa, ci metto un carattere di fine stringa, stampo, e ripristino
		printSubstring:
			#t8= ptr inizio
			#t0= ptr fine
			#t1= char estratto 
			
			move $t1,$zero 	#inizializzo				
			lb $t1, 0($t0)	#carico i char da salvare in t1

			#metto il carattere di fine stringa:
			move $t2,$zero
			sb $t2, 0($t0)	

			#Stampo la sottostringa:
			li	$v0, 4		#codice per stampare una stringa
			move $a0, $t8	#posizione stringa
			syscall 		#stampo
			
			#ripristino i char spostati:
			sb $t1, 0($t0)	

			#Vado a capo:
			li	$v0, 4		#codice per stampare una stringa	
			la	$a0, a_capo	#indico la stringa
			syscall 		#stampo

	#Stampata la sottostringa ho finito:
	jr $ra		 		# torno al chiamante 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
printReturnValue:	#PROCEDURA STAMPA VALORE DI RITORNO
	#a0= ID_func
	#a1= ReturnValue
	#Deve ritornare:
	#void

	#salvo ID e return value:
	move $t8, $a0	#t8=ID_func
	move $t9, $a1	#t9=Return value

	#Recupero il numero di indentazioni da fare
	la $t0, count_ind	#prendo l'indirizzo
	lw $t1, 0($t0)		#t1= count_ind
	addi $t1, $t1,-1	#decremento il counter per portarlo al valore giusto
	move $t2,$t1		#uso t2 per il loop, quindi t2=t1
	
	indent_loop:
		beq $t2,$zero, count_decr #se t2=0 ho finito il ciclo
		#Stampo una indentazione:
		li	$v0, 4		#codice per stampare una stringa	
		la	$a0, ind	#indico la stringa
		syscall 		#stampo
		#decremento il counter:
		addi $t2, $t2,-1	
		j indent_loop


	count_decr:	
		sw $t1, 0($t0)		#salvo il counter in memoria

		#ora stampo la freccia di uscita:
		li	$v0, 4				#codice per stampare una stringa	
		la	$a0, freccia_out	#indico la stringa
		syscall 				#stampo
 
	#scelgo il nome della funzione da stampare:
	#in t8 ho l'id della funzione
	#t0= valore di confronto
	li $t0, 'm'
	beq	$t8, $t0, loadSum	#somma

	li $t0, 't'
	beq	$t8, $t0, loadSub	#sottrazione

	li $t0, 'o'
	beq	$t8, $t0, loadProd	#prodotto

	li $t0, 'v'
	beq	$t8, $t0, loadDiv	#divisione


	loadSum:
		la $a0, strSomma	#carico la posizione della stringa
		j printChosed

	loadSub:
		la $a0, strSottr	#carico la posizione della stringa
		j printChosed

	loadProd:
		la $a0, strProd		#carico la posizione della stringa
		j printChosed

	loadDiv:
		la $a0, strDiv		#carico la posizione della stringa
		j printChosed


	printChosed:
		li	$v0, 4			#codice per stampare una stringa
		syscall 			#stampo

		la $a0, strReturn	#carico la posizione dell'altro pezzo di stringa da stampare
		li	$v0, 4			#codice per stampare una stringa
		syscall 			#stampo

		move $a0, $t9		#carico il return value
		li $v0,1			#codice per stampare un intero
		syscall 			#stampo

		la $a0, strClose	#carico la posizione dell'altro pezzo di stringa da stampare
		li	$v0, 4			#codice per stampare una stringa
		syscall 			#stampo

	#Ho finito, quindi:
	jr $ra		 		# torno al chiamante
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



#==================================================================================================
main: 	 #MAIN
	#in e out: void
	#####################################
	#PUSH nello Stack
	addi $sp, $sp, -4	#alloca memoria
	sw $ra, 0($sp)		#salva ra nello stack
	#####################################

	#carica la stringa da file
	# Open File
	open:
		li	$v0, 13		# Open File Syscall
		la	$a0, file	# Load File Name
		li	$a1, 0		# Read-only Flag
		li	$a2, 0		# (ignored)
		syscall
		move $t6, $v0	# Save File Descriptor
		blt	$v0, 0, err	# Goto Error
 
	# Read Data
	read:
		li	$v0, 14			# Read File Syscall
		move $a0, $t6		# Load File Descriptor
		la	$a1, buffer		# Load Buffer Address
		li	$a2, 152		# Buffer Size 
		syscall
 
	
	# Close File
	close:
		li	$v0, 16			# Close File Syscall
		move $a0, $t6		# Load File Descriptor
		syscall
		j	init			# Vado a manipolare i dati
 
	# Error
	err:
		li	$v0, 4		# Print String Syscall
		la	$a0, fnf	# Load Error String
		syscall

	init: #chiamo readString e inizio il programma:
		la $a0,buffer  		#mette in a0 il puntatore alla stringa da leggere
		jal readString      # chiama la procedura readString


	#########################################################
	#POP nello Stack: 
	lw $ra, 0($sp)		#riprendo ra

	addi $sp, $sp, 4	#dealloco
	jr $ra		 		# torno al chiamante (exeption handler)
	#########################################################
#==================================================================================================